import tensorflow as tf
import numpy as np
from six.moves import cPickle as pickle
import sys
import math
import time
from PIL import Image
from PIL import ImageEnhance
import PIL.ImageOps
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from collections import Counter
import itertools
import matplotlib.pyplot as plt
import os

PRED_FLAG= int(sys.argv[1])
file_in = int(sys.argv[2])

root_dir = os.environ['DOLPH_LANG_HOME'];

DATA_PATH = os.path.join(root_dir, 'data/pickles/')
DATA_FILE = DATA_PATH + 'sound_data.' + str(file_in) + '.pickle'

TEST_PATH = os.path.join(root_dir, 'data/sa13_147a_figs/')
DATA_TEST_FILE = TEST_PATH + 'art_data.pickle'

IMAGE_SIZE = 100
NUM_CHANNELS = 4
NUM_LABELS = 11
INCLUDE_TEST_SET = True 

class ConvNet:
	def __init__(self):
            '''Initialize the class by loading the required datasets 
            and building the graph'''
            self.load_pickled_dataset(DATA_FILE, DATA_TEST_FILE)

            self.graph = tf.Graph()
            self.define_tensorflow_graph()

	def define_tensorflow_graph(self):
            print '\nDefining model...'
            # Hyperparameters
            batch_size = 10
            learning_rate = 0.01
            layer1_filter_size = 5
            layer1_depth = 16
            layer1_stride = 2
            layer2_filter_size = 5
            layer2_depth = 16
            layer2_stride = 2
            layer3_num_hidden = 128
            num_training_steps = 3501

            # Add max pooling
            pooling = False 
            layer1_pool_filter_size = 2
            layer1_pool_stride = 2
            layer2_pool_filter_size = 2
            layer2_pool_stride = 2

            # Enable dropout and weight decay normalization
            dropout_prob = 0.5 # set to < 1.0 to apply dropout, 1.0 to remove
            weight_penalty = 0.01 # set to > 0.0 to apply weight penalty, 0.0 to remove

            with self.graph.as_default():
                # Input data
                tf_train_batch = tf.placeholder(tf.float32, shape=(batch_size, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))
                tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, NUM_LABELS))

                tf_valid_dataset = tf.constant(self.val_X)
                tf_test_dataset = tf.placeholder(tf.float32, shape=[len(self.test_X), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS])
                tf_train_dataset = tf.placeholder(
                        tf.float32, shape=[len(self.train_X), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS])

                # Implement dropout
                dropout_keep_prob = tf.placeholder(tf.float32)

                # Network weights/parameters that will be learned
                # Layer 1
                layer1_weights = tf.Variable(tf.truncated_normal(
                        [layer1_filter_size, layer1_filter_size, NUM_CHANNELS, layer1_depth], stddev=0.1)) 
                layer1_biases = tf.Variable(tf.zeros([layer1_depth]))
                layer1_feat_map_size = int(math.ceil(float(IMAGE_SIZE) / layer1_stride))
                print "Layer1:", layer1_biases.get_shape()

                if pooling:
                    layer1_feat_map_size = int(math.ceil(float(layer1_feat_map_size) / layer1_pool_stride))

                # Layer 2
                layer2_weights = tf.Variable(tf.truncated_normal(
                        [layer2_filter_size, layer2_filter_size, layer1_depth, layer2_depth], stddev=0.1))
                layer2_biases = tf.Variable(tf.constant(1.0, shape=[layer2_depth]))
                print "Layer2:", layer2_biases.get_shape()
                layer2_feat_map_size = int(math.ceil(float(layer1_feat_map_size) / layer2_stride))
                if pooling:
                    layer2_feat_map_size = int(math.ceil(float(layer2_feat_map_size) / layer2_pool_stride))

                # Layer 3
                layer3_weights = tf.Variable(tf.truncated_normal(
                        [layer2_feat_map_size * layer2_feat_map_size * layer2_depth, layer3_num_hidden], stddev=0.1))
                layer3_biases = tf.Variable(tf.constant(1.0, shape=[layer3_num_hidden]))
                print "Layer3:", layer3_biases.get_shape()

                # Layer 3
                layer4_weights = tf.Variable(tf.truncated_normal([layer3_num_hidden, NUM_LABELS], stddev=0.1))
                layer4_biases = tf.Variable(tf.constant(1.0, shape=[NUM_LABELS]))
                print "Layer4:", layer4_biases.get_shape()

                # Model
                def network_model(data):
                    '''Define the actual network architecture'''

                    # Layer 1
                    conv1 = tf.nn.conv2d(data, layer1_weights, [1, layer1_stride, layer1_stride, 1], padding='SAME')
                    hidden = tf.nn.relu(conv1 + layer1_biases)
                    print "hidden1:", hidden.get_shape()

                    if pooling: # TODO kill the false
                        hidden = tf.nn.max_pool(hidden, ksize=[1, layer1_pool_filter_size, layer1_pool_filter_size, 1], 
                                    strides=[1, layer1_pool_stride, layer1_pool_stride, 1],
                                    padding='SAME', name='pool1')
                
                    # Layer 2
                    conv2 = tf.nn.conv2d(hidden, layer2_weights, [1, layer2_stride, layer2_stride, 1], padding='SAME')
                    hidden = tf.nn.relu(conv2 + layer2_biases)
                    print "hidden2:", hidden.get_shape()

                    if pooling:
                        hidden = tf.nn.max_pool(hidden, ksize=[1, layer2_pool_filter_size, layer2_pool_filter_size, 1], 
                                    strides=[1, layer2_pool_stride, layer2_pool_stride, 1],
                                    padding='SAME', name='pool2')
                    
                    # Layer 3
                    shape = hidden.get_shape().as_list()
                    reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
                    hidden = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases)
                    hidden = tf.nn.dropout(hidden, dropout_keep_prob)
                    print "hidden3:", hidden.get_shape()
                    
                    # Layer 4 
                    output = tf.matmul(hidden, layer4_weights) + layer4_biases
                    print "output:", output.get_shape()
                    return output

                # Training computation
                logits = network_model(tf_train_batch)
                loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))

                # Add weight decay penalty
                loss = loss + weight_decay_penalty([layer3_weights, layer4_weights], weight_penalty)

                # Optimizer
                optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

                # Predictions for the training, validation, and test data.
                batch_prediction = tf.nn.softmax(logits)
                valid_prediction = tf.nn.softmax(network_model(tf_valid_dataset))
                test_prediction = tf.nn.softmax(network_model(tf_test_dataset))

                train_prediction = tf.nn.softmax(network_model(tf_train_dataset))

                def train_model(num_steps=num_training_steps):
                    '''Train the model with minibatches in a tensorflow session'''
                    with tf.Session( graph = self.graph ) as session:
                        tf.initialize_all_variables().run()
                        print 'Initializing variables...'

                        saver = tf.train.Saver()
                        
                        if PRED_FLAG == False:
                            max_acc = 0;

                            for step in range(num_steps):
                                offset = (step * batch_size) % (self.train_Y.shape[0] - batch_size)
                                batch_data = self.train_X[offset:(offset + batch_size), :, :, :]
                                batch_labels = self.train_Y[offset:(offset + batch_size), :]
                                
                                # Data to feed into the placeholder variables in the tensorflow graph
                                feed_dict = {tf_train_batch : batch_data, tf_train_labels : batch_labels, 
                                                            dropout_keep_prob: dropout_prob}
                                _, l, predictions = session.run([optimizer, loss, batch_prediction], feed_dict=feed_dict)

                                if (step % 100 == 0):
                                    train_preds = session.run(train_prediction, feed_dict={tf_train_dataset: self.train_X, dropout_keep_prob : 1.0})
                                    val_preds = session.run(valid_prediction, feed_dict={dropout_keep_prob : 1.0})
                                    print ''
                                    print('Batch loss at step %d: %f' % (step, l))
                                    print('Batch training accuracy: %.1f%%' % accuracy(predictions, batch_labels))
                                    print('Validation accuracy: %.1f%%' % accuracy(val_preds, self.val_Y))
                                    print('Full train accuracy: %.1f%%' % accuracy(train_preds, self.train_Y))

                                    acc = accuracy(val_preds, self.val_Y);
                                    if acc > max_acc:
                                        max_acc = acc;
                                        print "Saving model at val:", acc
                                        saver.save(session, 'my-model')

                            ckpt = tf.train.get_checkpoint_state('.')
                            if ckpt and ckpt.model_checkpoint_path:
                                saver.restore(session, ckpt.model_checkpoint_path)
                            else:
                                print "No checkpoint found!"

                            print "\n \n \n Obtaining final results on sets!"
                            
                            sets = [self.train_X, self.val_X, self.test_X]
                            targets = [self.train_Y, self.val_Y, self.test_Y]
                            data = [tf_train_dataset, tf_valid_dataset, tf_test_dataset]
                            pred = [train_prediction, valid_prediction, test_prediction]
                            set_names = ['Training data', 'Validation data', 'Test data'];
                            
                            names = ['wB', 'wA', 'wBB', 'wD3', 'wC', 'wD', 'wQ', 'wG', 'wH', 'wD2', 'wE2']

                            total = itertools.chain(np.argmax(targets[0], 1), np.argmax(targets[1], 1), np.argmax(targets[2], 1))

                            print "Count training:", Counter(np.argmax(targets[0], 1))
                            print "Count val:", Counter(np.argmax(targets[1], 1))
                            print "Count test:", Counter(np.argmax(targets[2], 1))
                            print "Total:", Counter(total)


                            for i in range(len(sets)):
                                preds = session.run(pred[i], 
                                        feed_dict={data[i]: sets[i], dropout_keep_prob : 1.0})
                                print 'Accuracy on', set_names[i], 'data: %.1f%%' % accuracy(preds, targets[i])
                                print(classification_report(np.argmax(targets[i], 1), np.argmax(preds, 1)))
                                print [(i, n) for i, n in enumerate(names)]


                        else:
                            ckpt = tf.train.get_checkpoint_state('.')
                            if ckpt and ckpt.model_checkpoint_path:
                                saver.restore(session, ckpt.model_checkpoint_path)
                            else:
                                print "No checkpoint found!"

                            sets = self.test_X
                            targets = self.test_Y
                            data = tf_test_dataset
                            pred = test_prediction
                            set_names = 'Test data';
                            names = np.array(['wB', 'wA', 'wBB', 'wD3', 'wC', 'wD', 'wQ', 'wG', 'wH', 'wD2', 'wE2'])

                            preds = session.run(pred, 
                                    feed_dict={data: sets, dropout_keep_prob : 1.0})
                            #print "test targets:", [(x, names[x]) for x in np.argmax(targets, 1)]
                            for i, x in enumerate(np.argmax(preds, 1)):
                                print preds[i]
                                print (names[x]), '\t',
                                # print self.test_names[i] #TODO come back for test

                            print(classification_report(np.argmax(targets, 1), np.argmax(preds, 1)))
                            print [(i, n) for i, n in enumerate(names)]
                            # save final preds to make confusion matrix

                            pred_ind = np.array([0, 1, 3, 5, 6, 7, 8, 10])
                            print names[pred_ind]
                            #matrx = confusion_matrix(np.argmax(targets, 1), np.argmax(preds, 1))
                            #self.plot_confusion_matrix(matrx, names[pred_ind])
                            print 'Accuracy on', set_names, 'data: %.1f%%' % accuracy(preds, targets)
                            self.final_val_preds = preds 
                    
                # save train model function so it can be called later
                self.train_model = train_model


	def load_pickled_dataset(self, pickle_file, pickle_file_test):
            print "Loading datasets..."
            
            with open(pickle_file, 'rb') as f:
                save = pickle.load(f)
                self.train_X = save['train_data']
                self.train_Y = save['train_labels']
                self.val_X = save['val_data']
                self.val_Y = save['val_labels']

                self.test_X = save['test_data']
                print self.test_X.shape
                self.test_X = np.concatenate((self.val_X, self.test_X), axis = 0)
                self.test_Y = save['test_labels']
                self.test_Y = np.concatenate((self.val_Y, self.test_Y), axis = 0)

                print self.test_X.shape
                print self.val_X.shape

            if PRED_FLAG and False: # TODO kill the false
                print('Pickle test file', pickle_file_test)
                with open(pickle_file_test, 'rb') as f:
                    save = pickle.load(f)
                    self.test_X = save['test_data']
                    self.test_Y = save['test_labels']
                    self.test_names = save['test_names']

            print 'Training set', self.train_X.shape, self.train_Y.shape
            print 'Validation set', self.val_X.shape, self.val_Y.shape
            print 'Test set', self.test_X.shape, self.test_Y.shape

            del save  # hint to help gc free up memory


        def plot_confusion_matrix(self, cm, classes,
                        normalize=True,
                        title='Confusion matrix',
                        cmap=plt.cm.Blues):

                """
                This function prints and plots the confusion matrix.
                Normalization can be applied by setting `normalize=True`.
                """
                size =  20
                plt.imshow(cm, interpolation='none', cmap=cmap)


                plt.title(title)
                plt.colorbar()
                tick_marks = np.arange(len(classes))
                plt.xticks(tick_marks, classes, rotation=45)
                plt.yticks(tick_marks, classes)

                if normalize:
                    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                    print("Normalized confusion matrix")
                else:
                    print('Confusion matrix, without normalization')

                print(cm)

                thresh = cm.max() / 2.
                for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                    plt.text(j, i, cm[i, j],
                                horizontalalignment="center",
                                color="white" if cm[i, j] > thresh else "black")

                plt.tight_layout()
                plt.ylabel('True label', fontsize=14)
                plt.xlabel('Predicted label', fontsize=14)

                plt.show()

def weight_decay_penalty(weights, penalty):
	return penalty * sum([tf.nn.l2_loss(w) for w in weights])

def accuracy(predictions, labels):
  return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])

if __name__ == '__main__':
    t1 = time.time()
    conv_net = ConvNet()
    conv_net.train_model()
    t2 = time.time()
    print "Finished training. Total time taken:", t2-t1
