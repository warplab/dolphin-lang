'''
Used to downsample spectrogram images and save them within a pickle file for 
the covolutional neural network features
Usage: python make_dataset.py <0/1 make a test dataset>
'''
import tensorflow as tf
import numpy as np
import os
import sys
import random
from PIL import Image
from PIL import ImageEnhance
import PIL.ImageOps
from six.moves import cPickle as pickle
from collections import Counter
import itertools

# Root dir where data files are stored
root_dir = os.environ['DOLPH_LANG_HOME'];

# Select whether to create a test file, or create a training only pickle file
create_test = int(sys.argv[1]);

# Parameters for the downsampled images
IMAGE_SIZE = 100 # In pixels by pixels
NUM_CHANNELS = 4 #RGB channels
PIXEL_DEPTH = 255.0
PICKLE_PATH = os.path.join(root_dir, 'data/pickles/')

if create_test: 
    NUM_SOUNDS = 1
    DATA_PATH = os.path.join(root_dir, 'data/sa13_147a_figs/')
    VALIDATION_PERCENT = 0.2
    TEST_PERCENT = 0.0
else:
    NUM_SOUNDS = 11 # TODO changed
    DATA_PATH = os.path.join(root_dir, 'data/bins/whistles/') # TODO changed
    VALIDATION_PERCENT = 0.10
    TEST_PERCENT = 0.10

def load_sound(sound):
    '''Load the images for a single sound.'''
    print "Loading images for sound", sound 
    folder = DATA_PATH + sound + '/'
    image_files = [x for x in os.listdir(folder) if '.png' in x]

    dataset = np.ndarray(shape=(len(image_files), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS),
                            dtype=np.float32)

    filenames =  []

    num_images = 0
    for image in image_files: image_file = os.path.join(folder, image) try:
            image_data = read_image_from_file(image_file) 
            dataset[num_images, :, :, :] = image_data 
            filenames.append(image_file)
            num_images = num_images + 1

        except IOError as e:
            print('Could not read:', image_file, ':', e)

    dataset = dataset[0:num_images, :, :, :] 
    print 'Sound dataset size:', dataset.shape
    print 'Mean:', np.mean(dataset) 
    print 'Standard deviation:', np.std(dataset)
    print '' 
    return dataset, filenames

def read_image_from_file(file_path):
    img = Image.open(file_path)
    img = img.resize((IMAGE_SIZE,IMAGE_SIZE), Image.ANTIALIAS) #downsample image
    pixel_values = np.array(img.getdata())
    return np.reshape(pixel_values, [IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS])

def make_dataset_arrays(num_rows=2000):
    data = np.ndarray((num_rows, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS), dtype=np.float32)
    labels = np.ndarray((num_rows, NUM_SOUNDS), dtype=np.int32)
    return data, labels

def randomize(dataset, labels, filenames = None):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation, :, :, :] # TODO 
    shuffled_labels = labels[permutation, :]
    """
    if filenames != None:
        shuffled_names = filenames[permutation]
        return shuffled_dataset, shuffled_labels, shuffled_names

    """
    return shuffled_dataset, shuffled_labels, [] 

def trim_dataset_arrays(data, labels, new_size):
    data = data[0:new_size, :, :, :]
    labels = labels[0:new_size, :]
    return data, labels

def scale_pixel_values(dataset):
    return (dataset - PIXEL_DEPTH / 2.0) / PIXEL_DEPTH

def make_basic_datasets(number):
    sound_path = DATA_PATH 
    sounds = [x for x in os.listdir(sound_path) if os.path.isdir(os.path.join(sound_path, x))]
    assert len(sounds) == NUM_SOUNDS

    train_data, train_labels = make_dataset_arrays()
    val_data, val_labels = make_dataset_arrays()
    test_data, test_labels = make_dataset_arrays()
    num_train = num_val = num_test = 0

    for label, sound in enumerate(sounds):
            # create a one-hot encoding of this sound's label
            sound_label = (np.arange(NUM_SOUNDS) == label).astype(np.float32)
            sound_data, filenames  = load_sound(sound)
            sound_data = scale_pixel_values(sound_data)
            num_sounds = len(sound_data)
            print "Sound Label (", label, sound, ") = ", sound_label
            print "Total figures:", num_sounds

            # randomly shuffle the data to ensure random validation and test sets
            filenames = np.array(filenames);

            assert(len(filenames) == len(sound_data))

            p = np.random.permutation(len(filenames))
            sound_data = sound_data[p] # TODO
            filenames = filenames[p]
            
            if create_test == False:
                nv = int(num_sounds * VALIDATION_PERCENT)

                # partition validation data
                sound_val = sound_data[0:nv, :, :, :]
                val_data[num_val:num_val+nv, :, :, :] = sound_val
                val_labels[num_val:num_val+nv, :] = sound_label
                num_val += nv

                # partition test data
                nt = int(num_sounds * TEST_PERCENT)
                sound_test = sound_data[nv:nv+nt, :, :, :]
                test_data[num_test:num_test+nt, :, :, :] = sound_test
                test_labels[num_test:num_test+nt, :] = sound_label
                num_test += nt

                # partition train data
                sound_train = sound_data[nv+nt:, :, :, :]
                ntr = len(sound_train)
                train_data[num_train:num_train+ntr, :, :, :] = sound_train
                train_labels[num_train:num_train+ntr, :] = sound_label
                num_train += ntr 

            else:
                num_test = 0;
                nt = int(num_sounds)
                sound_test = sound_data[0:nt, :, :, :]
                test_data[num_test:num_test+nt, :, :, :] = sound_test
                test_labels[num_test:num_test+nt, :] = sound_label
                num_test += nt

    # throw out extra allocated rows
    if create_test == False:
        train_data, train_labels = trim_dataset_arrays(train_data, train_labels, num_train)
        val_data, val_labels = trim_dataset_arrays(val_data, val_labels, num_val)
    
    test_data, test_labels = trim_dataset_arrays(test_data, test_labels, num_test)

    # shuffle the data to distribute samples from sounds randomly

    if create_test == False:
        train_data, train_labels, _ = randomize(train_data, train_labels)
        val_data, val_labels, _ = randomize(val_data, val_labels)

        print 'Training set:', train_data.shape, train_labels.shape
        print 'Validation:', val_data.shape, val_labels.shape

        train_arg = np.argmax(train_labels, 1)
        val_arg = np.argmax(val_labels, 1)

    test_data, test_labels, test_names = randomize(test_data, test_labels, filenames)

    print 'Testing:', test_data.shape, test_labels.shape
    test_arg = np.argmax(test_labels, 1)

    if create_test == False:
        total = itertools.chain(train_arg, val_arg, test_arg)
        print "Total:", Counter(total)
        print "Count training:", Counter(train_arg)
        print "Count val:", Counter(val_arg)

    print "Count test:", Counter(test_arg)

    # save all the datasets in a pickle file
    pickle_file = 'sound_data.' + str(number) + '.pickle'
    if create_test == False:
        save = {
            'train_data': train_data,
            'train_labels': train_labels,
            'val_data': val_data,
            'val_labels': val_labels,
            'test_data': test_data,
            'test_labels': test_labels
        }
    else:
        print "Test data:", test_data
        print "Test labels:", test_labels
        print "Test names:", test_names
        save = {
            'test_data': test_data,
            'test_labels': test_labels,
            'test_names': test_names
        }
            
    save_pickle_file(pickle_file, save)

def save_pickle_file(pickle_file, save_dict):
    try:
        f = open(PICKLE_PATH + pickle_file, 'wb')
        pickle.dump(save_dict, f, pickle.HIGHEST_PROTOCOL)
        f.close()
    except Exception as e:
        print('Unable to save data to', pickle_file, ':', e)
        raise

    print "Datasets saved to file", PICKLE_PATH + pickle_file

if __name__ == '__main__':
    print "Making sound dataset and saving it to:", PICKLE_PATH 
    print "To change this and other settings, edit the flags at the top of this file."

    # Number of cross validation data sets to make
    num_cross_sets = 10;

    for i in xrange(num_cross_sets):
        make_basic_datasets(i)
  	
