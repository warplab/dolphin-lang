"""
Utilities for the classify.py file
"""
import csv
from matplotlib import pyplot as plt
import scipy.io.wavfile as wav
import numpy as np
import string
from numpy.lib import stride_tricks
import numpy.matlib
from sklearn import datasets, linear_model
import sys
import os
import fcntl
import time

import sklearn.linear_model as lr
from sklearn import svm
from python_speech_features import mfcc
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score 
from fastdtw import fastdtw
from scipy.spatial.distance import euclidean
from sklearn import tree
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier

import make_mfcc_dataset

# Class that interfaces with the provided csv file of labels
class TargetGetter(object):
    def __init__(self):
        self._tag_ids = {} 
        self._seen = {}

        root_dir = os.environ['DOLPH_LANG_HOME'];
        with open(os.path.join(root_dir, 'data/ground_truth.csv'), 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter = ",")
            for row in reader:
                self._tag_ids[row[0]] = row[1]

    # ACCEPTS: a filename for a sound
    # MODIFIES: returns a label for the filename from the file 'data/updated_whistle_ground_truth.csv'
    # REQUIRES: the filename is of format 'saXXXx_t_t_label.wav'
    def get_target(self, filename):
        # Filename should be in format "tag_start(m)_start(s)_w.wav"
        print "Looking for:", filename, '\t',
        if filename in self._tag_ids:
            target = self._tag_ids[filename]
        else:
            target = "X";
            print "\n File not found! Assigning unknown label."
            return 'w' + target

        print "Loading:", filename, "with label:", target
        return 'w' + target

# Class for randomly generating a training, validation, and testing dataset
class GenerateDatasets(object):
    def __init__(self, X, y, names):
        self.X = X
        self.y = y
        self.names = np.array(names)

    # ACCEPTS: a random seed 
    # MODIFIES:  returns a randomly selected train, valid, test set with specified ratios
    # REQUIRES: seed is an positive integer, object already initialized with X, y datasets
    def generate(self, seed, one_hot_flag = False):
        # Generate the shuffler
        data_len = self.X.shape[0]
        sampler = np.arange(data_len)

        np.random.seed(seed)
        np.random.shuffle(sampler);

        # Can be modified to change ratios of training validation, and testing datasets
        train_len = int(data_len * 8.0/10.0)
        valid_len = int(data_len * 1.0/10.0)
        test_len = int(data_len * 1.0/10.0)

        print "Training set size(", train_len, "), Validation set size(", valid_len, "), Test set size(", test_len, ")"

        # Get the X data
        X_train = self.X[sampler[0:train_len], :]
        X_valid = self.X[sampler[train_len:train_len + valid_len], :]
        X_test = self.X[sampler[train_len + valid_len:], :]

        # Get the y data
        num_vocab = len(list(set(self.y)))
        word_to_id = {}
        id_to_word = {}
        index = 0;
        for y in self.y:
            if y not in word_to_id:
                word_to_id[y] = index;
                id_to_word[index] = y;
                index += 1;

        y_index = np.array([word_to_id[word] for word in self.y])

        y_train = y_index[sampler[0:train_len]]
        y_valid = y_index[sampler[train_len:train_len + valid_len]]
        y_test = y_index[sampler[train_len + valid_len:]]

        names_train = self.names[sampler[0:train_len]]
        names_valid = self.names[sampler[train_len:train_len + valid_len]]
        names_test = self.names[sampler[train_len + valid_len:]]

        # Save ID to word mappings in object
        self.word_to_id = word_to_id;
        self.id_to_word = id_to_word;
           
        return X_train, y_train, X_valid, y_valid, X_test, y_test, \
                names_train, names_valid, names_test

# Spectrogram object for generating, plotting, and saving spectrograms
class Spectogram(object):
    def __init__(self, samples, samplerate):
        self.samplerate = samplerate;
        self.samples = samples;
        self.binsize = 2 ** 10;

    """ plot spectrogram"""
    def plot(self, plotpath = None, true = "", pred = "",  colormap="jet"):
        s = self.stft(self.samples, self.binsize)
        
        sshow, freq = self.logscale_spec(s, factor=1.0, sr=self.samplerate)
        ims = 20.*np.log10(np.abs(sshow)/10e-6) # amplitude to decibel
        
        timebins, freqbins = np.shape(ims)
        
        #fig = plt.figure(figsize=(15, 7.5)) # Uncomment for figure frame
        fig = plt.figure(figsize=(15, 7.5), frameon=False) # Uncomment for raw figure
       
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off();
        fig.add_axes(ax);

        plt.imshow(np.transpose(ims), origin="lower", aspect="auto", cmap=colormap, interpolation="none")

        """ Uncomment for figures with axis labels
        plt.colorbar()

        plt.xlabel("time (s)")
        plt.ylabel("frequency (hz)")
        """

        plt.xlim([0, timebins-1])
        plt.ylim([0, freqbins])

        # Comment for figures without axis labels
        xlocs = np.float32(np.linspace(0, timebins-1, 5))
        plt.xticks(xlocs, ["%.02f" % l for l in ((xlocs*len(self.samples)/timebins)+(0.5*self.binsize))/self.samplerate])
        ylocs = np.int16(np.round(np.linspace(0, freqbins-1, 10)))
        plt.yticks(ylocs, ["%.02f" % freq[i] for i in ylocs])

        plt.title("True: " + str(true) + "Pred:" + str(pred))
        # End: figures with axis labels

        if plotpath:
            plt.savefig(plotpath, bbox_inches="tight")
        else:
            plt.show()
            
        plt.clf()

    """ short time fourier transform of audio signal """
    def stft(self, sig, frameSize, overlapFac=0.5, window=np.hanning):
        win = window(frameSize)
        hopSize = int(frameSize - np.floor(overlapFac * frameSize))
       
        # zeros at beginning (thus center of 1st window should be for sample nr. 0)
        samples = np.append(np.zeros((frameSize/2,)), sig)    
        # cols for windowing
        cols = np.ceil( (len(samples) - frameSize) / float(hopSize)) + 1
        # zeros at end (thus samples can be fully covered by frames)
        samples = np.append(samples, np.zeros(frameSize))
        
        frames = stride_tricks.as_strided(samples, shape=(cols, frameSize), strides=(samples.strides[0]*hopSize, samples.strides[0])).copy()
        frames *= win
        
        return np.fft.rfft(frames)    
    
    """ scale frequency axis logarithmically """    
    def logscale_spec(self, spec, sr=44100, factor=20.):
        timebins, freqbins = np.shape(spec)

        scale = np.linspace(0, 1, freqbins) ** factor
        scale *= (freqbins-1)/max(scale)
        scale = np.unique(np.round(scale))
        
        # create spectrogram with new freq bins
        newspec = np.complex128(np.zeros([timebins, len(scale)]))
        for i in range(0, len(scale)):
            if i == len(scale)-1:
                newspec[:,i] = np.sum(spec[:,scale[i]:], axis=1)
            else:        
                newspec[:,i] = np.sum(spec[:,scale[i]:scale[i+1]], axis=1)
        
        # list center freq of bins
        allfreqs = np.abs(np.fft.fftfreq(freqbins*2, 1./sr)[:freqbins+1])
        freqs = []
        for i in range(0, len(scale)):
            if i == len(scale)-1:
                freqs += [np.mean(allfreqs[scale[i]:])]
            else:
                freqs += [np.mean(allfreqs[scale[i]:scale[i+1]])]
        
        return newspec, freqs
        
# ACCEPTS: a sklearn model, a English string descriptoin of the model, and X, y datasets
# MODIFIES: prints prediction accuracy to console 
# REQUIRES: the model is an sklearn object with a "score" functionality
def run_scoring(model, string_label, X_train, y_train, X_valid, y_valid, X_test, y_test):
    loss_train = model.score(X_train, y_train)
    loss_valid = model.score(X_valid, y_valid)
    #loss_test = model.score(X_test, y_test) # TODO if you want testing uncomment

    print "------ ", string_label, "-------"
    print "Train accuracy:", loss_train
    print "Valid accuracy:", loss_valid
    #print "Test accuracy:", loss_test  # TODO if you want testing uncomment

# Config object for the neural network
class Config(object):
    # Should be the same as NUM_FEATS in the make_mfcc_dataset.py file
    nn_input_dim = 1508;
    # Should be the number of whistle classes
    nn_output_dim = 12

# Class for the Baseline classifiers, decision tree and discriminant function analysis
class BaselineClassifier(object):
    def trainBaseline_Tree(self, X, y):
        self.clf = tree.DecisionTreeClassifier(max_depth = 3);
        self.clf = self.clf.fit(X, y);

        return self.clf;
    
    def trainBaseline_DFA(self, X, y):
        self.dfa = LinearDiscriminantAnalysis()
        self.dfa.fit(X, y);

        return self.dfa

# Random forest class 
class RandomFClassifier(object):
    def trainRF(self, X, y):
        # Carry out training.
        self.rf = RandomForestClassifier(n_estimators = 10, max_depth = 3)
        self.rf = self.rf.fit(X,y);
        return self.rf;

# Logistic regression class
class LogisticClassifier(object):
    def trainLR(self, X, y, C = 1, max_iterations = 1000, reg = 'l2', dual = True):
        # Carry out training.
        self._logreg = lr.LogisticRegression(reg, C = C, fit_intercept = True, intercept_scaling = 1, \
                multi_class = 'multinomial', max_iter = max_iterations, solver = 'newton-cg');

        self._logreg.fit(X,y);
        return self._logreg;

# SVM class
class SVMClassifier(object):
    
    def trainSVM_gauss(self, X, y, C):
        # Carry out training.
        self.SVM = svm.SVC(C = 1., gamma = C);
        self.SVM.fit(X, y)
        return self.SVM;
    
    def trainSVM_poly(self, X, y, C):
        # Carry out training.
        self.SVM = svm.SVC(kernel = 'poly', C = C);
        self.SVM.fit(X, y)
        return self.SVM;

# KNN and DTW class
class ClusterClassifier(object):
    def __init__(self, trainX, trainy):
        self.X_train = trainX
        self.y_train = trainy 
        
    def knn(self, X_test, y_test):
        preds = []
        
        for i, test_point in enumerate(X_test):
            min_dist = float("inf")
            closest_seq = []

            for j, X_train in enumerate(self.X_train):
                distance, path = fastdtw(X_train, test_point, dist=euclidean);
                if distance < min_dist:
                    min_dist = distance
                    closest_seq = j;

            print "Test point", i, "of", len(X_test), "dist:", min_dist
            preds.append(self.y_train[closest_seq])

        print classification_report(y_test, preds)
        return accuracy_score(y_test, preds) 

# Custom neural network implementation
class NeuralNetworkClassifier(object):
    def __init__(self, X, y, X_val, y_val, X_test, y_test, \
            names_train, names_valid, names_test, hidden):
        self.X = X
        self.y = y
        self.X_val = X_val
        self.y_val = y_val
        self.X_test = X_test
        self.y_test = y_test
        self.names_train = names_train
        self.names_test = names_test
        self.names_valid = names_valid

        self.nn_hidden = hidden;

        self.print_loss = True

    def visualize(self, X, y, names, nn_params, view_flag):
        count = 0;
        bad_files = [];
        true = []
        pred = []
        for i in xrange(X.shape[1]): 
            guess = self.predict(nn_params, X[:, i]); 
            if guess == y[i]:
                count += 1; 
            elif view_flag == 1:
                bad_files.append(names[i])
                true.append(y[i])
                pred.append(guess)
                #print "Name:", names[i], "Guess:", guess, "Actual:", y[i] 
        print "Accuracy:", count / float(X.shape[1]),
        return bad_files, true, pred

    # Helper function to evaluate the total loss on the dataset
    def calculate_loss(self, nn_params):
        W1, b1, W2, b2 = nn_params['W1'], nn_params['b1'], nn_params['W2'],nn_params['b2']
        num_examples = self.X.shape[1]  # training set size
        
        # Forward propagation
        z1 = np.dot(W1.T, self.X) + b1
        a1 = np.maximum(z1, 0)
        z2 = np.dot(W2.T, a1) + b2

        avg = np.amax(z2, axis=0)
        avg_mat = np.matlib.repmat(avg, Config.nn_output_dim, 1)
        exp_scores = np.exp(z2 - avg_mat)
            
        softmax = exp_scores / np.sum(exp_scores, axis=0, keepdims=True)
        
        # Calculating the loss
        correct_logprobs = -np.log(softmax[self.y, range(num_examples)])
        return np.sum(correct_logprobs) / num_examples

    def predict(self, nn_params, x):
        W1, b1, W2, b2 = nn_params['W1'], nn_params['b1'], nn_params['W2'],nn_params['b2']
        # Forward propagation
        x = x.reshape(Config.nn_input_dim,1);
        z1 = np.dot(W1.T, x) + b1;
        a1 = np.maximum(z1, 0)
        z2 = np.dot(W2.T, a1) + b2
        
        avg = np.amax(z2, axis=0)
        avg_mat = np.matlib.repmat(avg, Config.nn_output_dim, 1)
        exp_scores = np.exp(z2 - avg_mat)

        probs = exp_scores / np.sum(exp_scores, axis=0, keepdims=True)

        return np.argmax(probs, axis=0)

    # This function learns parameters for the neural network and returns the model.
    # - nn_hidden: Number of nodes in the hidden layer
    # - num_passes: Number of passes through the training data for gradient descent
    # - print_loss: If True, print the loss every 1000 iterations

    def build_model(self, num_passes = 100000, print_loss = False):
        X_val, y_val = self.X_val, self.y_val

        #X_val, y_val = read_val_data();
        # Initialize the parameters to random values. We need to learn these.
        num_examples = self.X.shape[1]

        np.random.seed(8)
        # W1 is 2 x 5
        W1 = np.random.normal(0, 1.0/np.sqrt(Config.nn_input_dim), (Config.nn_input_dim, self.nn_hidden, 1)).reshape(Config.nn_input_dim, self.nn_hidden) 
        # b1 is a 5 x 1 
        b1 = np.zeros((self.nn_hidden, 1))  

        # W2 is 5 x 3
        W2 = np.random.normal(0, 1.0/np.sqrt(self.nn_hidden), (self.nn_hidden, Config.nn_output_dim,  1)).reshape(self.nn_hidden, Config.nn_output_dim) 
        # b2 is 3 x 1 
        b2 = np.zeros((Config.nn_output_dim, 1))
        
        # This is what we return at the end
        self.nn_params = {}
    
        epoch = 0;
        # Gradient descent. For each batch...
        #for i in range(0, num_passes):
        fl = fcntl.fcntl(sys.stdin.fileno(), fcntl.F_GETFL)
        fcntl.fcntl(sys.stdin.fileno(), fcntl.F_SETFL, fl | os.O_NONBLOCK)

        while True:
            #To allow ayncrnous stopping with enter key
            try:
                stdin = sys.stdin.read()
                if "\n" in stdin or "\r" in stdin:
                    break
            except IOError:
                pass
            #To allow ayncrnous stopping with enter key

            epoch += 1; 
            sample_num = np.random.randint(0, num_examples);

            X = self.X[:, sample_num].reshape(Config.nn_input_dim, 1)

            # Forward propagation
            z1 = np.dot(W1.T, X) + b1
            a1 = np.maximum(z1, 0)
            z2 = np.dot(W2.T, a1) + b2
            #Can add more layers here to increases hidden dimensions

            #To prevent overflow
            avg = np.amax(z2); 
            output_scores = np.exp(z2 - avg)
            softmax = output_scores /  np.matlib.repmat(np.sum(output_scores, axis = 0), Config.nn_output_dim, 1)

            # Backpropagation
            delta2 = softmax;

            delta2[self.y[sample_num], 0] -= 1

            gradient = np.zeros(self.nn_hidden);
            gradient[z1[:, 0] > 0] = 1;
            temp = np.dot(np.diag(gradient), W2)
            delta1 = np.dot(temp, delta2); 

            dW2 = np.dot(a1, delta2.T)
            db2 = delta2

            dW1 = np.dot(X, delta1.T)
            db1 = delta1

            # Gradient descent parameter update
            # TRIED ADAPTIVE LEARNING RATE
            Config.epsilon = (10 + epoch/1000.0) ** -0.5
            #Config.epsilon =  10.1

            W1 += -Config.epsilon * dW1
            b1 += -Config.epsilon * db1
            W2 += -Config.epsilon * dW2
            b2 += -Config.epsilon * db2
            
            # Assign new parameters to the model
            self.nn_params = {'W1': W1, 'b1': b1, 'W2': W2, 'b2': b2}
            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if self.print_loss and epoch % 1000== 0:
                #print("Loss after iteration %i: %f" % (epoch, self.calculate_loss(nn_params, self.X, self.y)))
                print "Loss after iteration ", epoch, "Train:", 
                self.visualize(self.X, self.y, self.names_train, self.nn_params, 0)
                print "Val:", 
                self.visualize(self.X_val, self.y_val, self.names_valid, self.nn_params, 0)
                print "Test:", 
                self.visualize(self.X_test, self.y_test, self.names_test,  self.nn_params, 0)
                print
                print "Press the return key at any time to end training."

            
        return self.nn_params

    def run(self):
        # MAIN
        X_train, y_train = self.X, self.y
        X_test, y_test = self.X_test, self.y_test

        hidden_nodes = self.nn_hidden;
        
        nn_params = self.build_model(print_loss = True)
        print "Test:", 
        return self.visualize(X_test, y_test, self.names_test,  nn_params, 1)



