# Dolphin Language Classification

Includes code to train various classifiers, CNN code, MFCC and feature extraction, image data extraction, along with dolphin vocalization datasets and labels.
Note: Code is still under development, so please contact geflaspo@mit.edu with any questions.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Clone the directory onto your local machine and move into the cloned directory.
``` 
git clone git@gitlab.com:warplab/dolphin-lang.git 
cd dolphin-lang
```
### Prerequisites

This code is written in Python 2. Use of the neural classifiers requires the installation of Tensorflow with Python 2 support. 

Installation instructions can be found [here](https://www.tensorflow.org/install/).

### Setup the directory structure

1. Before running any code, set the path to the outer directory that contains the code and data files. Run in a terminal:
    ```
    export DOLPH_LANG_HOME="<path to data outer directory>"
    e.g. export DOLPH_LANG_HOME="/home/genevieve/mit-whoi/dolph-lang"
    ```
    You have to run this command every time you open a new terminal. To avoid this, add the line to your bashrc.
    Open `~/.bashrc` in your favorite editor and paste the export line at the bottom. Save.
    
2. Create a folder in the DOLPH_LANG_HOME named `data`. Within data, create a folder named `clear_clip`'. Place your data files in `DOLPH_LANG_HOME/data/clear_clips`.

3. Place a csv file containing data file names and associated data in `DOLPH_LANG_HOME/data`. CSV file should read like this:
    ```
    FILENAME, LABEL
    file1.wav,A
    file2.wav,B
    ...
    ```
    Name this file `ground_truth.csv`

4. Within the data folder, create a folder names `whistles`. For each class in your classification problem, create a folder within `whistles` with the name of the class i.e. `data/whistles/wA` and place all of the images or spectrograms of that class within the directory. e.g.
    ```
   DOLPH_LANG_HOME/data/whistles/wB/whistleB1.png
   DOLPH_LANG_HOME/data/whistles/wB/whistleB2.png
   DOLPH_LANG_HOME/data/whistles/wC/whistleC.png
    ...
    ```

## Running the classification code

### To run classify.py:
1. Place '.wav' clips in the folder `DOLPH_LANG_HOME/data/clear_clips`
2. Open `classify.py` and change line 22 to `create_pickles = 1`
    This will tell the algorithm to generate the datasets and save in pickled file. For successive runs of the classifier, this flag can be set to 0 to use the  pickled dataset, which will run much faster then creating the dataset from scratch. This will create a feature dataset saved as data.pickle
3. `python classify.py <random seed> <do-knn and NN flag>`
    The knn classifier and neural net are slow, so if you don't want to run them, set this flag to False. Set random seed to any integer. Runs with the same random seed will create the same test/validation/train shufflings. Runs with differnt random seeds will have differnt shufflings of the dataset.

### To run conv.py:
Before running this code, you must install the Tensorflow Machine Learning Library with Python 2 support.
1. Place '.png' spectrogram images in the `DOLPH_LANG_HOME/data//whistles` folder as sepcified above.
2. `python make_dataset.py`
    By defualt, the code will create 1 randomly shuffled cross-validation dataset. Change line 232 to an integer greater than 1 to make multiple, random, cross validation datasets. 
3. `python convolutional-neural-network.py <PRED_FLAG> <file_in>`
    Set `PRED_FLAG` to 0 when training the model for the first time and choose `file_in` to be between 0 and the number of cross validation datasets created previously. e.g.
    
    ```
    python convolutional-neural-network.py 0 5
    ```
4. The above command will train the network for a set number of epochs and then compute the training, testing, and validation accuracies. It will also save the model with highest validation accuracy in a model file. To test the model again, without retraining, run with `PRED_FLAG` flag set to true.

## Authors

* [**Genevieve Flaspohler**](http://geflaspohler.com)

## License

This project is licensed under the MIT License.

## References and Acknowledgments
Citaitons for the code and the labeled dolphin whistle dataset are respectively:
Dolphin whistle classification code: 
> Flaspohler, G. E., Silva, T., Mooney, T. A., & Girdhar, Y. (2017). A convolutional neural network based approach for classifying delphinidae vocal repertoire. The Journal of the Acoustical Society of America, 141(5), 3864-3864.

Labeled pantropical spotted dolphin whistle dataset:
> Silva, T. L., Mooney, T. A., Sayigh, L. S., Tyack, P. L., Baird, R. W., & Oswald, J. N. (2016). Whistle characteristics and daytime dive behavior in pantropical spotted dolphins (Stenella attenuata) in Hawai ‘i measured using digital acoustic recording tags (DTAGs). The Journal of the Acoustical Society of America, 140(1), 421-429.


