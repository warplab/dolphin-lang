'''
Sample code for using Fast Dyamic Time warping
'''

import numpy as np
from matplotlib import pyplot as plt
import mlpy
import matplotlib.cm as cm
from fastdtw import fastdtw
from scipy.spatial.distance import euclidean

def distance_cost_plot(distances):
    im = plt.imshow(distances, interpolation='nearest', cmap='Reds') 
    plt.gca().invert_yaxis()
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.grid()
    plt.colorbar();

x = np.array([1, 1, 2, 3, 2, 0])
y = np.array([0, 1, 1, 2, 3, 2, 1])

distances = np.zeros((len(y), len(x)))

for i in xrange(len(y)):
    for j in xrange(len(x)):
        distances[i, j] = (x[j] - y[i]) ** 2

dist, path = fastdtw(x, y, dist=euclidean)

fig = plt.figure(1)
ax = fig.add_subplot(111)
plot2 = plt.plot(path[0], path[1], 'w')

plt.show()

print dist

