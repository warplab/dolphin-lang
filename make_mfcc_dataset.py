import sys
import os
import pickle

import csv
from matplotlib import pyplot as plt
import scipy.io.wavfile as wav
import numpy as np
from numpy.lib import stride_tricks
import wave
import pyaudio
from python_speech_features import mfcc

import programUtils as progutil 

root_dir = os.environ['DOLPH_LANG_HOME'];

# PREFORMS: Read's sound files, generages MFCC features
# REQUIRES: Sound files should be found as '.wav' in data/clear_clips

def get_and_pickle_data(plot_to_screen):
    # Parameters
    data_dir = os.path.join(root_dir, 'data/clear_clips')

    # For most ML algorithms, every training example must be the same
    # dimension. Because the set of auido clips may have different
    # lengths, the MFCC features extracted from the clips will also have
    # different lengths. This can be remedied either by padding all clips
    # to be the same length as the longest clip or clipping all clips
    # to be the same length as the shortest clip. This code shortens all
    # clips to the same length by cropping around the middle of clip.  

    num_files = len(os.listdir(data_dir))
 
    # Find minimum length clip
    min_length_clip = float("inf")
    for i, filename in enumerate(os.listdir(data_dir)):
        Fs, audio_data = wav.read(os.path.join(data_dir, filename));
        if len(audio_data) < min_length_clip:
            min_length_clip = len(audio_data)
            shortest_file = filename

    Fs, audio_data = wav.read(os.path.join(data_dir, shortest_file));
    mfcc_feat = np.array(mfcc(signal=audio_data, samplerate=Fs, numcep=26))
    num_samps = mfcc_feat.shape[0];
    min_length_clip = num_samps;

    mfcc_feat = mfcc_feat[num_samps/2 - min_length_clip/2: num_samps/2 + min_length_clip/2]
    mfcc_feat_long = np.reshape(mfcc_feat, (1, mfcc_feat.shape[0] * mfcc_feat.shape[1]), 'F')
    NUM_FEATS = mfcc_feat_long.shape[1]
    print "Number of MFCC features per clip:", NUM_FEATS

    # Set up global data structures
    X = np.zeros((num_files, NUM_FEATS))
    y = []
    names = []

    getTarget = progutil.TargetGetter();

    # Import all clips
    for i, filename in enumerate(os.listdir(data_dir)):
        Fs, audio_data = wav.read(os.path.join(data_dir, filename));

        target = getTarget.get_target(filename)  # TODO 

        # Get MFCC features, truncated from middle based on shortest clips, and 
        # reshape into a matrix
        mfcc_feat = np.array(mfcc(signal=audio_data, samplerate=Fs, numcep=26))
        num_samps = mfcc_feat.shape[0];  
        mfcc_feat = mfcc_feat[num_samps/2 - min_length_clip/2: num_samps/2 + min_length_clip/2]
        mfcc_feat_long = np.reshape(mfcc_feat, (1, mfcc_feat.shape[0] * mfcc_feat.shape[1]), 'F')

        # Reshape mfcc_feat_long to fit in X matrix
        counter = range(0, mfcc_feat_long.shape[1]);
        mfcc_feat_long = (mfcc_feat_long.T[counter]).T
        X[i, :] = np.array(mfcc_feat_long);
    
        # Append the clip label and filenames
        y.append(target)
        names.append(filename)

    X = X[0:len(y), :];  # Truncated X to length of y, important for testing

    # Save data to pickle file for later use
    if not os.path.exists(os.path.join(root_dir, 'data/pickles')):
        os.makedirs(os.path.join(root_dir, 'data/pickles'))

    with open(os.path.join(root_dir, 'data/pickles/data.pickle'), 'w') as f:
        pickle.dump([X, y, names], f);
