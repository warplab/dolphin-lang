'''
Read's a sound file in from DATA_PATH, and plot to scree
'''
from PIL import Image
from PIL import ImageEnhance
import PIL.ImageOps
import numpy as np
import os

NUM_CHANNELS = 4
root_dir = os.environ['DOLPH_LANG_HOME'];
DATA_PATH = os.path.join(root_dir, 'data/bins/wB/sa147d_980_2_w.png')

def read_image_from_file(file_path):
    img = Image.open(file_path)
    h, w = img.size
    pixel_values = np.array(img.getdata())
    return np.reshape(pixel_values, [w, h, NUM_CHANNELS])

if __name__ == '__main__':
	data = read_image_from_file(DATA_PATH)
        im_int = np.uint8(data)
        im = Image.fromarray(im_int)
        im.show();
