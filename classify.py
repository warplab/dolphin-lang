"""
The main classification file for all non-CNN classification, using MFCC 
features or baseline features. Can be run using the following command:
    classify.py <random seed for dataset creation> <0/1 for do DTW>
"""
import sys
import os
import pickle

from matplotlib import pyplot as plt
import scipy.io.wavfile as wav
import numpy as np
from numpy.lib import stride_tricks
import wave
import pyaudio

import programUtils as progutil 
import make_mfcc_dataset as make_pickles

import csv

root_dir = os.environ['DOLPH_LANG_HOME'];

# Sound files should be found as '.wav' in data/clear_clips
# Image spectrograms will be saved in 'data/bins/<sound_label>' as '.png'
# These sound label folders must already exsist in data/bins
if __name__ == '__main__':
    # Parameters
    seed = int(sys.argv[1]) # Set random seed for generating datasets
    do_knn = int(sys.argv[2]) # Select whether to do time conusming KNN calc and NN

    create_pickles = 1 # Select weather to create pickeled datasets; run once

    # Regularizaiton constants for logistic regression, SVM guassian kernal, 
    # and polynomial kernel repsectively. Sets number of hidden nodes in NN
    C_lr = 0.0001
    C_svm = 0.1
    C_svm_poly = 0.00000001
    nn_hidden = 400 

    # Read in '.wav' files and create pickled datasets
    # Saves pickled files in data/pickles/data.pickle
    if create_pickles:
        make_pickles.get_and_pickle_data(plot_to_screen=False)

    # Opens pickled dataset (assuming pickles have been created)
    with open(os.path.join(root_dir, 'data/pickles/data.pickle'), 'r') as f:
        Xgot, ygot, namesgot = pickle.load(f);

    # Generate training, validation, and testing datasets randomly
    dataOrganizer = progutil.GenerateDatasets(Xgot, ygot, namesgot);
    X_train, y_train, X_valid, y_valid, X_test, y_test, \
            names_train, names_valid, names_test = dataOrganizer.generate(seed);

    # Declare classifier objects
    BaselineClassifier = progutil.BaselineClassifier();
    RF = progutil.RandomFClassifier();
    LR = progutil.LogisticClassifier();
    SVM = progutil.SVMClassifier();
    NN = progutil.NeuralNetworkClassifier(X_train.T, y_train, X_valid.T, y_valid, \
            X_test.T, y_test, names_train, names_valid, names_test,  nn_hidden);

    # Training and Scoring 
    # Decision tree classifier
    dtree = BaselineClassifier.trainBaseline_Tree(X_train, y_train);
    progutil.run_scoring(dtree, 'Decision Tree', X_train, y_train,  X_valid, y_valid, X_test, y_test)

    # Discrimintant Analysis classifier 
    dfa = BaselineClassifier.trainBaseline_DFA(X_train, y_train);
    progutil.run_scoring(dfa, 'Discriminant Analysis', X_train, y_train, X_valid, y_valid,  X_test, y_test)
    
    # Random forest classifier 
    rf = RF.trainRF(X_train, y_train);
    progutil.run_scoring(rf, 'Random Forest', X_train, y_train, X_valid, y_valid,  X_test, y_test)

    # Logistic regression classifier
    logreg = LR.trainLR(X_train, y_train, C_lr); 
    progutil.run_scoring(logreg, 'Logistic Regression', X_train, y_train, X_valid, y_valid, X_test, y_test)

    # SVM polynomical kernel classifier
    svm_object_poly = SVM.trainSVM_poly(X_train, y_train, C_svm_poly);
    progutil.run_scoring(svm_object_poly, 'SVM Poly', X_train, y_train, X_valid, y_valid, X_test, y_test)

    # SVM gaussian kernel classifier
    svm_object_gauss = SVM.trainSVM_gauss(X_train, y_train, C_svm);
    progutil.run_scoring(svm_object_gauss, 'SVM Gauss', X_train, y_train, X_valid, y_valid, X_test, y_test)

    if do_knn:
        # Feed forward neural network classifier
        bad_files, true, pred = NN.run(); # Run NN

        # KNN and DTW classifier
        KNN_DTW = progutil.ClusterClassifier(X_train, y_train);
        print "------ KNN DTW -------"
        knn_score_test = KNN_DTW.knn(X_test, y_test)
        print "Test accuracy:", knn_score_test 
        knn_score_train = KNN_DTW.knn(X_train, y_train)
        print "Train accuracy:", knn_score_train 
        knn_score_valid = KNN_DTW.knn(X_valid, y_valid)
        print "Valid accuracy:", knn_score_valid 

    # Uncomment to make a string of predictions based on trained models
    # progutil.make_prediction(svm_object_poly, dataOrganizer)
    
